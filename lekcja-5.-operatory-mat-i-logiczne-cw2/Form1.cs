﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lekcja_5._operatory_mat_i_logiczne_cw2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnZaloguj_Click(object sender, EventArgs e)
        {
            const string login = "admin";
            const string haslo = "admin";

            string podanyLogin = txtLogin.Text;
            string podaneHaslo = txtHaslo.Text;

            /* Operator "&&" (i) zwraca tylko prawdę, kiedy obydwa wyrażenia też zwracają prawdę */
            bool czyPoprawne = (login == podanyLogin && haslo == podaneHaslo);

            MessageBox.Show("Czy podales poprawne dane? " + czyPoprawne.ToString(), "Logowanie", MessageBoxButtons.OK);
        }
    }
}
